### Prerequisites

* Basic knowledge of computer operating

### Course Content

#### Day 1

* Introduction to C Programming
* System setup
* C compilation process
* Basic Linux Commands
* Data Types, Operators & Conditional Statements

#### Day 2

* Loops
* Functions
* Introduction to Pointers

#### Day 3

* Arrays
* Pointer & Arrays
* Strings

#### Day 4

* Structure & Union
* Files
* Project - Combining everything together

#### Day 5

* Project - Combining everything together